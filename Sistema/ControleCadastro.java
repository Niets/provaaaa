package Sistema;

import java.util.ArrayList;

public class ControleCadastro {
    
    private ArrayList<Cadastro> umaListaCadastro;
    private String mensagem;

    public ControleCadastro() {
        umaListaCadastro = new ArrayList<Cadastro>();
    }
    
    public String adicionarCadastro(Cadastro umCadastro){
        
        mensagem = "Cadastro realizado com sucesso!"; 
        umaListaCadastro.add(umCadastro);
        
        return mensagem;
    }
    
    public String deletarCadastro(Cadastro umCadastro){
        
        mensagem = "Cadastro removido com sucesso";
        umaListaCadastro.remove(umCadastro);
        
        return mensagem;        
    }
    
    
    public void alterarStatus (String codTecnico){
        
        Cadastro umCadastro = null;
        umCadastro.setStatus(false);
        
    }
    
    
}
