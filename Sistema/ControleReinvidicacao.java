package Sistema;

import java.util.ArrayList;

public class ControleReinvidicacao {
    
    private ArrayList<Reinvidicacao> umaListaReinvidicacao;
    private Reinvidicacao umaReinvidicacao;
    private String mensagem;
    private String armazenarMensagem;
    
    public ControleReinvidicacao() {
        umaListaReinvidicacao = new ArrayList<Reinvidicacao>();
        umaReinvidicacao = new Reinvidicacao();
    }

    public ArrayList<Reinvidicacao> getUmaListaReinvidicacao() {
        return umaListaReinvidicacao;
    }

    public void setUmaListaReinvidicacao(ArrayList<Reinvidicacao> umaListaReinvidicacao) {
        this.umaListaReinvidicacao = umaListaReinvidicacao;
    }
    
    public String denunciarReinvidicacao(Reinvidicacao umaReinvidicacao){
        
        mensagem = "Denúncia feita com sucesso!";
        umaReinvidicacao.setNumDenunciado(umaReinvidicacao.getNumDenunciado() +1);
        
        return mensagem;        
    }
            
    public String aprovarReinvidicacao(Reinvidicacao umaReinvidicacao){
        
        mensagem = "Reinvidicação aprovada pelo governante!";
        umaReinvidicacao.setAprovadoPolitico(true);
        
        return mensagem;
    }
    
    public String enviarReinvidicacao(Reinvidicacao umaReinvidicacao){
        
        mensagem = "Reinvidicação enviada com sucesso!";
        umaListaReinvidicacao.add(umaReinvidicacao);
        
        
        return mensagem;
        
    }
    
    public String deletarReinvidicação(Reinvidicacao umaReinvidicacao, String CodTecnico){
        
        
        mensagem = "Reinvidicação deletada com sucesso!";
        umaListaReinvidicacao.remove(umaReinvidicacao);
               
        return mensagem;
    }
    
    public void verificarReinvidicacao(Reinvidicacao umaReinvidicacao){
        
        umaReinvidicacao.setVisualizacao(umaReinvidicacao.getVisualizacao() +1);
        
    }
    
    public String editarReinvidicacao(Reinvidicacao umaReinvidicacao, String texto){
        
        armazenarMensagem = umaReinvidicacao.getComentario();
        umaReinvidicacao.setComentario(texto);
        mensagem = "Reinvidicação alterada de: "  + armazenarMensagem + " para: " + texto;
        
        return mensagem;
    }
            
            
}
