package Sistema;
import Usuario.Cidadao;

public class Reinvidicacao {
    
    private Cidadao autor;
    private String data;
    private String comentario;
    private int visualizacao;
    private boolean verificar;
    private int numDenunciado;
    private boolean aprovadoPolitico;

    public boolean isAprovadoPolitico() {
        return aprovadoPolitico;
    }

    public void setAprovadoPolitico(boolean aprovadoPolitico) {
        this.aprovadoPolitico = aprovadoPolitico;
    }

    public Cidadao getAutor() {
        return autor;
    }

    public void setAutor(Cidadao autor) {
        this.autor = autor;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getNumDenunciado() {
        return numDenunciado;
    }

    public void setNumDenunciado(int numDenunciado) {
        this.numDenunciado = numDenunciado;
    }

    public boolean isVerificar() {
        return verificar;
    }

    public void setVerificar(boolean verificar) {
        this.verificar = verificar;
    }

    public int getVisualizacao() {
        return visualizacao;
    }

    public void setVisualizacao(int visualizacao) {
        this.visualizacao = visualizacao;
    }
}
