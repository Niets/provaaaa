
import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class CadastroTest {
	
	private Cadastro umCadastro = new Cadastro();	
	
	@Test
	public void testNome() {
		umCadastro.setNome("Tester");
		assertEquals(umCadastro.getNome(), "Tester");
	}
	
	@Test
	public void testnick() {
		umCadastro.setNick("Tester");
		assertEquals(umCadastro.getNick(), "Tester");
	}
	
	@Test
	public void testEstado() {
		umCadastro.setEstado("GO");
		assertEquals(umCadastro.getEstado(), "GO");
	}
	
	@Test
	public void testNumPosts() {
		umCadastro.setNumPosts(2);
		assertEquals(umCadastro.getNumPosts(), 2);
	}
	

}
