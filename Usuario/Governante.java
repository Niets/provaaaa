package Usuario;

public class Governante extends Pessoa {
    
    private String codGovernante;

    public String getCodGovernante() {
        return codGovernante;
    }

    public void setCodGovernante(String codGovernante) {
        this.codGovernante = codGovernante;
    }
    
}
